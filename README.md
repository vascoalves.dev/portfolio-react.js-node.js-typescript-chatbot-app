# TEMPERATURE CONVERTER CHATBOT

This project is a simple chat application (chatbot) that assists the user in converting temperature values ​​between celsius, fahrenheit and kelvin.

## Getting Started

- You will have to open two terminals to run the backend and the frontend at the same time
- Check if you have the correct directory for the backend (..\chatApp\chat-app-backend) and for the frontend (..\chatApp\chat-app)
- Now you can run the following scripts for both terminals:

```bash
npm install
```

Followed by:

```bash
npm start
```

### Usage

You start by choosing the unit you want to convert from by entering it into the input area.
1. You have three options and you can change them whenever you enter one of them
    - "C" (celsius)
    - "F" (fahrenheit)
    - "K" (kelvin)
2. Then you enter the value you want to convert

### Author

VASCO ALVES - Software Developer\
08/2023