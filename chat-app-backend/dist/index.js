"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// src/index.ts
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const chatController_1 = __importDefault(require("./controllers/chatController"));
const app = (0, express_1.default)();
const port = 3001;
// Server
app.use((0, cors_1.default)());
app.use('/api/chat/', body_parser_1.default.json(), chatController_1.default);
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
