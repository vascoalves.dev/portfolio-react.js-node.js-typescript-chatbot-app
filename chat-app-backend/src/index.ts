// src/index.ts
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import chatController from './controllers/chatController';

const app = express();
const port = 3001;

// Server

app.use(cors());

app.use('/api/chat/', bodyParser.json(), chatController);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
