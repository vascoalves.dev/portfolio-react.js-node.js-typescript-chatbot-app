// src/controllers/chatController.ts
import bodyParser from 'body-parser';
import express from 'express';

// Handles client messages

const router = express.Router();

var unit = '';

router.post('/bot', bodyParser.json(), (req, res) => {
  const userMessage = req.body.message;

  console.log(userMessage);

  let response : string = '';

  // algorithm to check for valid messages
  if(userMessage.toLowerCase() !== 'c' || 
      userMessage.toLowerCase() !== 'f' ||
        userMessage.toLowerCase() !== 'k') {
          response = "Invalid Unit provided. You can choose from 'C' (Celsius), 'F' (Fahrenheit)  or 'K' (Kelvin).";
  }

  if(userMessage.toLowerCase() === 'c' || 
      userMessage.toLowerCase() === 'f' ||
        userMessage.toLowerCase() === 'k') {
          unit = userMessage;
          response = 'Give me a temperature in ' + userMessage.toUpperCase() + '.';
  }

  if(!unit) {
    response = "Give me a Unit first. You can choose from 'C' (Celsius), 'F' (Fahrenheit)  or 'K' (Kelvin).";
  } else {
    if(!isNaN(parseFloat(userMessage))) {
      response = convertTemperature(parseFloat(userMessage), unit)
      + '. Give me another unit or value.';
    }
  }

  // Return the bot's response
  res.json({ message: response });
});

// Conversion function for temperatures
function convertTemperature(value: number, unit: string): string {
  switch (unit.toUpperCase()) {
    case 'C':
      const celsius = value;
      const fahrenheit = (value * 9/5) + 32;
      const kelvin = value + 273.15;
      return `${value}°C is equal to ${fahrenheit.toFixed(2)}°F and ${kelvin.toFixed(2)}K`;
    case 'F':
      const celsiusF = (value - 32) * 5/9;
      const fahrenheitF = value;
      const kelvinF = (value + 459.67) * 5/9;
      return `${value}°F is equal to ${celsiusF.toFixed(2)}°C and ${kelvinF.toFixed(2)}K`;
    case 'K':
      const celsiusK = value - 273.15;
      const fahrenheitK = (value * 9/5) - 459.67;
      const kelvinK = value;
      return `${value}K is equal to ${celsiusK.toFixed(2)}°C and ${fahrenheitK.toFixed(2)}°F`;
    default:
      return 'Invalid unit provided.';
  }
}

export default router;
