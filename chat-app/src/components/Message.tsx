// Message.tsx
import React from 'react';

// React component for the 2 types of messages

interface MessageProps {
  text: string;
  isUser: boolean;
}

const Message: React.FC<MessageProps> = ({ text, isUser }) => {
  const messageClassName = isUser ? 'User-message' : 'Bot-message';

  return (
    <div className={`Chat-message ${messageClassName}`}>
      {text}
    </div>
  );
};

export default Message;
