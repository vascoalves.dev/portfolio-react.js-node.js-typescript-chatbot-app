// ChatMessageList.tsx
import React from 'react';
import Message from './Message';

interface ChatMessageListProps {
  messages: Array<{ text: string; isUser: boolean }>;
}

// React Component for every message
const ChatMessageList: React.FC<ChatMessageListProps> = ({ messages }) => {
  return (
    <div className="Chat-message-list">
      {messages.map((message, index) => (
        <Message key={index} text={message.text} isUser={message.isUser} />
      ))}
    </div>
  );
};

export default ChatMessageList;