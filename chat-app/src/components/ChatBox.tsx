// ChatBox.tsx
import React, { useState } from 'react';
import ChatMessageList from './ChatMessageList';
import ChatInput from './ChatInput';

// Sets history and new messages in chat

const ChatBox: React.FC = () => {
  const [messages, setMessages] = useState<Array<{ text: string; isUser: boolean }>>([]);
  const [inputText, setInputText] = useState<string>('');

  const handleSendMessage = async (message: string) => {
    setMessages(messages => [
      ...messages,
      { text: message, isUser: true }
    ]);
    setInputText(message);

    const response = await fetch('http://localhost:3001/api/chat/bot', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ message })
    });

    const responseData = await response.json();

    setMessages(prevMessages => [...prevMessages, { text: responseData.message, isUser: false }]);

    // Workaround - scrolls all the way to the bottom after receiving last message from server
    setTimeout( () => ScrollToBottom(), 0);
  };

  function ScrollToBottom(){
    var component = document.querySelector(".Chat-container");
    if(component) {
      component.scrollTop = component.scrollHeight;
    }
  }

  return (
    <div className="chat-box">
      <ChatMessageList messages={messages} />
      <div className='input'>
        <ChatInput onSendMessage={handleSendMessage} />
      </div>
    </div>
  );
};

export default ChatBox;