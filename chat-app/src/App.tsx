import React from 'react';
import './App.css';
import Message from './components/Message';
import ChatBox from './components/ChatBox';

// Main React Component
const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <h1>TEMPERATURE CONVERTER CHATBOT</h1>
      </header>
      <div className="App-container">
        <div className="Chat-container">
          <div className='Chat-message'>
            <Message text="Hello! I am the Temperature Converter ChatBot! Enter 'C' to enter a value in Celcius, 'F' to enter a value in Fahrenheit or 'K' to enter a value in Kelvin." isUser={false} />
          </div>
          <ChatBox />
        </div>
      </div>
    </div>
  );
};

export default App;
